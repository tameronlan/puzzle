class Controller {
    constructor(props = {}){
        this.dispatch(props);
    }

    dispatch(props = {}){
        this.request = props.request;
        this.result = props.result;
        this.next = props.next;
        this.path = props.path;
        this.data = this.result.locals;

        this.data.title = '';

        app.set('views',  `${appPath}/app/layout`);
    }

    render(action = false, props = {}){
        this.data.pagePath = false;

        if ( action ){
            this.data.pagePath = [appPath, 'app', this.path, 'tpl', action].join('/');
        }

        this.result.render('web');
    }
}

module.exports = Controller;