let BaseController = require(`${appPath}/core/Controller`);

class BlogController extends BaseController{
    constructor(props){
        super(props);
    }

    index(){
        this.render("index");
    }
}

module.exports = BlogController;